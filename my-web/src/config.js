import {set as setConfig} from '$ui/config' 
// const {
//   Vue, 
//   // progress,
//   Access
// } = require('$ui/index')
// 写入运行时配置
setConfig({

  /**
   * 路由模式
   */
  router: {
    mode: 'hash',
    base: '/'
  },

  /**
   * 是否应用自动生成的路由配置
   */
  autoRoutes: true,

  /**
   * 权限控制配置
   */
  access: {
    // 启用请求权限控制
    axios: true,

    // 启用路由权限控制
    router: true, 
  

    // 缓存存储方式 session 或 local
    storage: 'session',

    // 登录页面路径
    loginPath: '/login',

    // 权限不足页面路径
    authorizePath: '/403',

    response: function(vm, options, config) {
      if (config.data.status === 401 || config.data.status === 403) {
        this.logout()
        setTimeout(() => {
          window.location.reload()
        }, 500)
      } else {
        // 在这里实现对请求前的处理
        return config
      }

      
    },

    // 请求拦截函数，axios=true 有效
    request: function({access}, options, config) { 
      config.headers.authorization = access.token || '' 
      // 在这里实现对请求前的处理
      return config
    }
  },

  /**
   * 开启访问分析追踪
   */
  analysis: true,

  /**
   * 属性名映射
   */
  keys: {
    code: 'code',
    data: 'data',
    message: 'msg',
    total: 'total',
    list: 'list',
    page: 'page',
    limit: 'pageSize',
    pages: 'pages'
  }
})

