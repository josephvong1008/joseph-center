import Vue from 'vue'
import Main from './index.vue'

const ToastConstructor = Vue.extend(Main)
let instance

const Toast = function(options = {}) {
 
  instance = new ToastConstructor({
    data: options
  }).$mount()
  
  document.body.appendChild(instance.$el)
}

Toast.success = (options) => {
  options.type = 'success'
  return Toast(options)
}
export default Toast