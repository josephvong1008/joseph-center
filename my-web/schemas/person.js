module.exports = {
  vuex: false,
  model: [
    {
      path: '/person',
      title: '人员模块',
      name: 'personModel',
      methods: ['fetch', 'add', 'update', 'get', 'remove'] // 
    }
  ]
}