module.exports = {
  vuex: false,
  model: [
    {
      path: '/user',
      title: '用户模块',
      name: 'userModel',
      methods: ['fetch', 'add', 'update', 'get', 'remove']
    },
    {
      path: '/login',
      title: '用户登录',
      name: 'login',
      methods: false,
      options: {
        method: 'post'
      }
    },
    {
      path: '/signin',
      title: '用户注册',
      name: 'signin',
      methods: false,
      options: {
        method: 'post'
      }
    },
    {
      path: '/user/insertMany',
      title: '用户模块测试',
      name: 'userInsertMany',
      methods: false,
      options: {
        method: 'get'
      }
    },
    {
      path: '/user/deleteMany',
      title: '用户模块测试',
      name: 'userDeleteMany',
      methods: false,
      options: {
        method: 'get'
      }
    }
  ]
}