module.exports = {
  vuex: false,
  model: [
    {
      path: '/testType',
      title: '标签模块',
      name: 'testTypeModel',
      methods: ['fetch', 'add'] // , 'update', 'get', 'remove'
    }
  ]
}