module.exports = {
  vuex: true,
  model: [
    {
      path: '/testItem',
      title: '标签模块',
      name: 'testItemModel',
      methods: ['fetch', 'add'] // , 'update', 'get', 'remove'
    }
  ]
}