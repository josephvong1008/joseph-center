module.exports = {
  vuex: false,
  model: [
    {
      path: '/file/upload',
      title: '文件模块上传',
      name: 'fileUpload',
      methods: false,
      options: {
        method: 'post',
        headers: {
          'Content-Type': 'multipart/form-data'
        }
      }
    },
    {
      path: '/file/download',
      title: '文件模块下载',
      name: 'fileDownload',
      methods: false,
      options: {
        method: 'post'
      }
    },
    {
      path: '/file/downloadById',
      title: '文件模块下载',
      name: 'fileDownloadById',
      methods: false,
      options: {
        method: 'get'
      }
    },
    {
      path: '/file/uploadBigFile',
      title: '文件模块大文件上传',
      name: 'uploadBigFile',
      methods: false,
      options: {
        method: 'post'
      }
    },
    {
      path: '/file/uploadBigFileFinish',
      title: '文件模块大文件上传完成',
      name: 'uploadBigFileFinish',
      methods: false,
      options: {
        method: 'post'
      }
    },
    {
      path: '/file/downloadBigFile',
      title: '文件模块大文件上传完成',
      name: 'downloadBigFile',
      methods: false,
      options: {
        method: 'post'
      }
    }
  ]
}