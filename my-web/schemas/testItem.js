module.exports = {
  vuex: false,
  model: [
    {
      path: '/testItem',
      title: '标签模块',
      name: 'testItemModel',
      methods: ['fetch', 'add'] // , 'update', 'get', 'remove'
    },
    {
      path: '/testItem/calcItemCount',
      title: '标签模块',
      name: 'calcItemCount',
      methods: false,
      options: {
        method: 'get'
      }
    }
  ]
}