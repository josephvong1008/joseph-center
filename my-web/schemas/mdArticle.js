module.exports = {
  vuex: false,
  model: [
    {
      path: '/mdArticle',
      title: '文章模块',
      name: 'articleModel',
      methods: ['fetch', 'add', 'update', 'get', 'remove'] // 
    },
    {
      path: '/mdArticle/groupArticleByTag',
      title: '根据标签类型查找文章',
      name: 'groupMdArticleByTag',
      methods: false,
      options: {
        method: 'get'
      }
    }
  ]
}