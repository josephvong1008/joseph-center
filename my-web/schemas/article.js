module.exports = {
  vuex: false,
  model: [
    {
      path: '/article',
      title: '文章模块',
      name: 'articleModel',
      methods: ['fetch', 'add'] // , 'update', 'get', 'remove'
    },
    {
      path: '/article/groupArticleByTag',
      title: '文章模块',
      name: 'groupArticleByTag',
      methods: false,
      options: {
        method: 'get'
      }
    }
  ]
}