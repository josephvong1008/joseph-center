module.exports = {
  vuex: false,
  model: [
    {
      path: '/role',
      title: '用户模块',
      name: 'roleModel',
      methods: ['fetch', 'add']
    }
  ]
}