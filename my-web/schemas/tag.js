module.exports = {
  vuex: false,
  model: [
    {
      path: '/tag',
      title: '标签模块',
      name: 'tagModel',
      methods: ['fetch', 'add', 'update', 'remove'] // 'get', 'remove'
    },
    {
      path: '/tag/findByUser',
      title: '标签模块',
      name: 'findTagByUser',
      methods: false, // 'add', 'update', 'get', 'remove'
      options: {
        method: 'get'
      }
    }
  ]
}