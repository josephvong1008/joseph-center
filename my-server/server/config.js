const path = require("path")
const config = {
    email: {
        host: 'smtp.qq.com',
        auth: {
            user: '2416118450@qq.com',
            pass: 'uffgpbimwxccebie',
        },
        recipients: 'abc_xf@126.com'
    }
}
const STATIC_ROUTE = '/file'
const STATIC_PATH = path.join(__dirname, '../', 'file')
const FILE_PATH = '/uploads'
const DEPLOY_PATH = path.join(__dirname, '../', 'web')
module.exports = {
    PORT: 3087,
    CORS: {
        origin: false,
        exposeHeaders: ['WWW-Authenticate', 'Server-Authorization', 'Date'],
        maxAge: 100,
        credentials: true,
        allowMethods: ['GET', 'POST', 'OPTIONS', 'PATCH', 'DELETE'],
        allowHeaders: ['Content-Type', 'Authorization', 'Accept', 'X-Custom-Header', 'anonymous']
    },
    LOGINFO: {
        appenders: {
            info: {
                type: "DateFile",
                category: 'dateFileLog',
                filename: path.join(__dirname, './log/info/'),
                pattern: "yyyy-MM-dd.log",
                alwaysIncludePattern: true
            },
            email: {
                type: '@log4js-node/smtp',
                 //发送邮件的邮箱
                sender: config.email.auth.user,
                 //标题
                subject: 'Latest error report',
                SMTP: {
                    host: config.email.host, 
                    auth: config.email.auth,
                },
                recipients: config.email.recipients
            }
        },
        categories: {
            default: { appenders: ['info'], level: 'info' },
            error: { appenders: ['info', 'email'], level: 'error' },
        }
    },
    TOKEN_KEY: 'JOSEPH_CENTER', // token加解密密钥
    TOKEN_EXPIRE: 60, // token过期时间单位（分钟）
    HOST: 'http://127.0.0.1',
    STATIC_PATH: STATIC_PATH,
    STATIC_ROUTE: STATIC_ROUTE,
    UPLOAD_PATH: path.join(STATIC_PATH, FILE_PATH),
    DOWNLOAD_PATH: path.join(STATIC_ROUTE, FILE_PATH),
    DOWNLOAD_ROUTE: FILE_PATH,

    DEPLOY_PATH: DEPLOY_PATH // 部署路径
}