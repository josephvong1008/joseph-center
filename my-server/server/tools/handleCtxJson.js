// 此工具以弃用，用于作为 中间件 案例参考 
const parse = require('./url.js').parse
function paresPostData(ctx){
  return new Promise((resolve,reject)=>{
    try{
      let postData=''
      ctx.req.addListener('data',(data)=>{
        postData+=data
      })
      ctx.req.on('end', ()=>{
          resolve(postData)
      })

    }catch(err){
        reject(err)
    }
  })
}


const func = function (ctx, next) { 
  return async (ctx, next) => {
    if(ctx.method === 'POST') {
      const postData = await paresPostData(ctx)  
      ctx.request.body = parse(postData)   
    }
    await next() 
  }  
} 

module.exports = func