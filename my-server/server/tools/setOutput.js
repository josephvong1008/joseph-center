const date = require('./date')
const standardObj = function (obj) {
  const data = obj && {...obj} 
  data.createdTime = data.createdTime ? date(data.createdTime, 'yyyy-MM-dd hh:mm:ss') : ''
  data.updateTime = data.updateTime ? date(data.updateTime, 'yyyy-MM-dd hh:mm:ss') : ''
  data.id = data._id
  delete data._id
  return data
}
const setOutput = function (data, type) {
  let output = null
  if (type === 'fetch') { 
    const payload = data.list.map((item) => {
      return standardObj(item)
    })
    output = {
      list: payload,
      total: data.total
    }
  } else {
    if (typeof data === 'object') {
      output = standardObj(data)
    } else {
      output = data
    }
  }
  return {
    data: output ,
    code: 0,
    msg: '成功'
  }
}

// module.exports = setOutput
exports.setOutput = setOutput
exports.standardObj = standardObj