const mongoose = require('mongoose');  
const { Schema, model } = mongoose;

// 用户的数据库模型  
let TagSchema = new Schema({
  label: { type: String, required: true },
  desc: { type: String }
  // articles: { type: Array }
}, {
  timestamps: { createdAt: 'createdTime', updatedAt: 'updateTime' },
  id: false
});

module.exports = model('TagSchema', TagSchema);