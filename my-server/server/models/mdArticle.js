const mongoose = require('mongoose');  
const { Schema, model } = mongoose;

// 用户的数据库模型  
let MdArticleSchema = new Schema({
  title: { type: String, required: true },
  mdText: { type: String },
  mdContent: { type: String },
  tags: [{
    type: mongoose.Schema.ObjectId,
    ref: 'TagSchema'
  }], 
  createUserName: { type: String, required: true },
  createName: { type: String, required: true },
  createId: { type: String, required: true },
  imgs: { type: Array }
}, {
  timestamps: { createdAt: 'createdTime', updatedAt: 'updateTime' },
  id: false
});

module.exports = model('MdArticleSchema', MdArticleSchema);