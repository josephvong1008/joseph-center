const mongoose = require('mongoose');  
const { Schema, model } = mongoose;

// 用户的数据库模型  
let PersonSchema = new Schema({
  name: { type: String, required: true },
  age: { type: Number },
  gender: { type: String },
  city: { type: String },
  startDate: { type: String },
  endDate: { type: String }
}, {
  timestamps: { createdAt: 'createdTime', updatedAt: 'updateTime' },
  id: false
});

module.exports = model('PersonSchema', PersonSchema);