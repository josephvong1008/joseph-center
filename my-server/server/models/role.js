const mongoose = require('mongoose');  
const { Schema, model } = mongoose;

// 用户的数据库模型  
let RoleSchema = new Schema({
  roleName: { type: String, required: true, unique: true },
  _id: { type: String, required: true, unique: true },
  roleId: { type: String, required: true, unique: true },
  accessIds: { type: Array, required: true }
}, {
  timestamps: { createdAt: 'createdTime', updatedAt: 'updateTime' },
  id: false
});

module.exports = model('RoleSchema', RoleSchema);