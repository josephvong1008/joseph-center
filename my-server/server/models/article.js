const mongoose = require('mongoose');  
const { Schema, model } = mongoose;

// 用户的数据库模型  
let ArticleSchema = new Schema({
  content: { type: String, required: true },
  title: { type: String },
  tags: [{
    type: mongoose.Schema.ObjectId,
    ref: 'TagSchema'
  }], 
  createUserName: { type: String, required: true },
  createName: { type: String, required: true },
  createId: { type: String, required: true },
  attachments: { type: Array }
}, {
  timestamps: { createdAt: 'createdTime', updatedAt: 'updateTime' },
  id: false
});

module.exports = model('ArticleSchema', ArticleSchema);