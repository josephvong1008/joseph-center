const mongoose = require('mongoose');  
const { Schema, model } = mongoose;

// 用户的数据库模型  
let FileSchema = new Schema({
  fileName: { type: String, required: true, unique: true }, 
  originName: { type: String, required: true }, 
  url: { type: String, require: true, unique: true },
  _id: { type: String, require: true, unique: true },
  isRelated: {type: Boolean, require: true}
}, {
  timestamps: { createdAt: 'createdTime', updatedAt: 'updateTime' },
  id: true
});

module.exports = model('FileSchema', FileSchema);