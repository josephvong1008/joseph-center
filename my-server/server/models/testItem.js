const mongoose = require('mongoose');  
const { Schema, model } = mongoose;

// 用户的数据库模型  
const TestItemSchema = new Schema({
  content: { type: String },
  title: { type: String, required: true  },
  types: [{
    type: mongoose.Schema.ObjectId,
    ref: 'TestType'
  }]
}, {
  timestamps: { createdAt: 'createdTime', updatedAt: 'updateTime' },
  id: false
});

module.exports = model('TestItem', TestItemSchema);