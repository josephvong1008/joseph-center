const mongoose = require('mongoose');  
const { Schema, model } = mongoose;

// 期号的数据库模型  
let TestTypeSchema = new Schema({
    name: { type: String, required: true, unique: true },
    desc: { type: String }
});

module.exports = model('TestType', TestTypeSchema);