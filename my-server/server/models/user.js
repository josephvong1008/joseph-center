const mongoose = require('mongoose');  
const { Schema, model } = mongoose;

// 用户的数据库模型  
let UsreSchema = new Schema({
  userName: { type: String, required: true, unique: true },
  password: { type: String, require: true },
  roles: { type: Array, require: true },
  name: { type: String },
  age: { type: Number }
}, {
  timestamps: { createdAt: 'createdTime', updatedAt: 'updateTime' },
  id: true
});

module.exports = model('UsreSchema', UsreSchema);