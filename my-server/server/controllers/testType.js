const TestType = require('../models/testType')
const setOutput = require('../tools/setOutput').setOutput
const standardObj = require('../tools/setOutput').standardObj
class TestTypeController {
  prefix = '' 
  getPath(name){
    return name ? `/${this.prefix}/${name}` : `/${this.prefix}`
  }  

  async find(ctx, next) {
    const {page = 1, pageSize = 20} = ctx.request.query 
    const total = await TestType.count({})
    const list = await TestType.find().skip((parseInt(page) - 1) * parseInt(pageSize)).limit(parseInt(pageSize) || 5).lean()
    const data = {
      list: list,
      total: total
    }
    ctx.response.body = setOutput(data, 'fetch')
  }

  async add(ctx, next){  
    ctx.verifyParams({
      name: { type: "string", required: true }
    })
    let obj = {...ctx.request.body}
    
    const [err, data] = await to(new TestType(obj).save())
    if(err) return ctx.throw(500, err)
    ctx.response.body = setOutput(data.toJSON())  

  }
}


module.exports = new TestTypeController()