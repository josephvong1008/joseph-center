const articleController = require('../controllers/article')
const Router = require('koa-router')
const router = new Router({
	prefix: '/api'
})
 
articleController.prefix = 'article'

router.post(articleController.getPath(), articleController.add)
.get(articleController.getPath(), articleController.find)
.get(articleController.getPath('groupArticleByTag'), articleController.groupArticleByTag) 


module.exports = router