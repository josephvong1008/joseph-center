const testItemController = require('../controllers/testItem')
const Router = require('koa-router')
const router = new Router({
	prefix: '/api'
})
 
testItemController.prefix = 'testItem'

router.post(testItemController.getPath(), testItemController.add)
.get(testItemController.getPath(), testItemController.find)
.get(testItemController.getPath('calcItemCount'), testItemController.calcItemCount)

module.exports = router