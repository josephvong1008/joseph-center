const personController = require('../controllers/person')
const Router = require('koa-router')
const router = new Router({
	prefix: '/api'
})

personController.prefix = 'person'
router.post(personController.getPath(), personController.add)
.get(personController.getPath(), personController.find)
.patch(personController.getPath(), personController.update) 
.get(personController.getPath(`:id`), personController.findOne) 
.delete(personController.getPath(':id'), personController.delete)

module.exports = router