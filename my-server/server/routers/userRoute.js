const userController = require('../controllers/user')
const Router = require('koa-router')
const router = new Router({
	prefix: '/api'
})

userController.prefix = 'user'
router.post(userController.getPath(), userController.add)
.get(userController.getPath(), userController.find)
.patch(userController.getPath(), userController.update)
.get(userController.getPath('insertMany'), userController.insertMany) // 测试接口
.get(userController.getPath('deleteMany'), userController.deleteMany) // 测试接口
.get(userController.getPath(`:id`), userController.findOne)
.delete(userController.getPath(':id'), userController.delete)


module.exports = router