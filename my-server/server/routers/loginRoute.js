const loginController = require('../controllers/login')
const Router = require('koa-router')
const router = new Router({
	prefix: '/api'
})
 
router.post('/login', loginController.login)
router.post('/signin', loginController.signin)
 
module.exports = router