const router = require('koa-router')()
router.get('/hello', ctx => {
  ctx.body = 'hello!2'
})


const userRoute = require('./userRoute')
const tagRoute = require('./tagRoute') 
const roleRoute = require('./roleRoute') 
const loginRoute = require('./loginRoute')
const fileRoute = require('./fileRoute') 
const articleRoute = require('./articleRoute')  
const mdArticleRoute = require('./mdArticleRoute') 
const personRoute = require('./personRoute') 
// -------- 以下两个模块为测试用 ----------------
const testTypeRoute = require('./testTypeRoute')
const testItemRoute = require('./testItemRoute')
const testRoute = require('./test')

module.exports = function(app) {
  app.use(router.routes(), router.allowedMethods());
  // ------------- 正式API ----------------
  app.use(userRoute.routes(), userRoute.allowedMethods())
  app.use(tagRoute.routes(), tagRoute.allowedMethods())
  app.use(loginRoute.routes(), loginRoute.allowedMethods())
  app.use(roleRoute.routes(), roleRoute.allowedMethods()) 
  app.use(fileRoute.routes(), fileRoute.allowedMethods())
  app.use(articleRoute.routes(), articleRoute.allowedMethods())
  app.use(mdArticleRoute.routes(), mdArticleRoute.allowedMethods())  
  app.use(personRoute.routes(), personRoute.allowedMethods()) 


  // ----------测试api------
  app.use(testTypeRoute.routes(), testTypeRoute.allowedMethods())
  app.use(testItemRoute.routes(), testItemRoute.allowedMethods())
  app.use(testRoute.routes(), testRoute.allowedMethods())
}