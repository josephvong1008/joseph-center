const testTypeController = require('../controllers/testType')
const Router = require('koa-router')
const router = new Router({
	prefix: '/api'
})
 
testTypeController.prefix = 'testType'

router.post(testTypeController.getPath(), testTypeController.add)
.get(testTypeController.getPath(), testTypeController.find)
 

module.exports = router