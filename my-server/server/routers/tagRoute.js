const tagController = require('../controllers/tag')
const Router = require('koa-router')
const router = new Router({
	prefix: '/api'
})
 
tagController.prefix = 'tag'

router.get(tagController.getPath(), tagController.find)
      .post(tagController.getPath(), tagController.add)
      .patch(tagController.getPath(), tagController.update)
      .delete(tagController.getPath(`:id`), tagController.delete)
      // .get(tagController.getPath('findByUser'), tagController.findByUser)


module.exports = router