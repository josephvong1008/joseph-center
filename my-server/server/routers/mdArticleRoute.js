const mdArticleController = require('../controllers/mdArticle')
const Router = require('koa-router')
const router = new Router({
	prefix: '/api'
})

mdArticleController.prefix = 'mdArticle'
router.post(mdArticleController.getPath(), mdArticleController.add)
.get(mdArticleController.getPath(), mdArticleController.find)
.patch(mdArticleController.getPath(), mdArticleController.update)
.get(mdArticleController.getPath('groupArticleByTag'), mdArticleController.groupArticleByTag)
.get(mdArticleController.getPath(`:id`), mdArticleController.findOne) 
.delete(mdArticleController.getPath(':id'), mdArticleController.delete)



module.exports = router