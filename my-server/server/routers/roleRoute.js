const roleController = require('../controllers/role')
const Router = require('koa-router')
const router = new Router({
	prefix: '/api'
})
 
roleController.prefix = 'role'
router.post(roleController.getPath(), roleController.add)
      .get(roleController.getPath(), roleController.find)
      .get(roleController.getPath('deleteMany'), roleController.deleteMany)
 
module.exports = router