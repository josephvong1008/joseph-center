const fileController = require('../controllers/file')
const Router = require('koa-router')
const router = new Router({
	prefix: '/api'
}) 
fileController.prefix = 'file'

router.post(fileController.getPath('upload'), fileController.upload) 
router.post(fileController.getPath('download'), fileController.download) 
router.get(fileController.getPath(`downloadById`), fileController.downloadById)
router.post(fileController.getPath('uploadBigFile'), fileController.uploadBigFile) 
router.post(fileController.getPath('uploadBigFileFinish'), fileController.uploadBigFileMerge)
 

module.exports = router