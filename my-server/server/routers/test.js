const Router = require('koa-router')
const router = new Router({
	prefix: '/api'
})
 
// testItemController.prefix = 'test'
// name ? `/${this.prefix}/${name}` : `/${this.prefix}`

router.get('/test', async (ctx, next) => {
  console.log(ctx.request.query)
  ctx.response.body = {
    status: 'ok'
  }
})
 

module.exports = router