# npm依赖
npm install koa koa-router koa-bodyparser koa2-cors koa-parameter koa-json-error await-to-js log4js @log4js-node/smtp --save
npm i koa-body -S
npm install mongoose --save

# HTTP
HTTP status
200 正常
442 校验错误
500 服务器错误

# 启动、关闭数据库
使用管理员打开cmd， 运行以下命令
net start josephDB 
net stop josephDB

# 启动服务
npm run dev