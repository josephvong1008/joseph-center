const Koa = require('koa')
const KoaStaticCache = require('koa-static-cache')
const Router = require('koa-require')

const app = new Koa()
const router = new Router()

